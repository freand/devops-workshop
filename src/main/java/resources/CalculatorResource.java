package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String calculate(String expression){
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+","");

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         * 
         * Returns error message if no match is found.
         */
        if(expressionTrimmed.matches("^\\d+(?:\\s*[+]\\s*\\d+)*$")) return String.valueOf(sum(expressionTrimmed));
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[-]\\s*\\d+)*$")) return String.valueOf(subtraction(expressionTrimmed));
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[*]\\s*\\d+)*$")) return String.valueOf(multiplication(expressionTrimmed));
        else if(expressionTrimmed.matches("^\\d+(?:\\s*[/]\\s*\\d+)*$")){
            int divResult = division(expressionTrimmed);
            if (divResult == -1) return "Du kan ikke dele på 0";
            return String.valueOf(divResult);
        }
        else {return expressionTrimmed + " er ikke et gyldig regnestykke!";}
    }
    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */ 
    public int sum(String expression){
        String[] split = expression.split("[+]");
        int splitSize = split.length;
        int number = 0;

        for (String n : split) {
            number += Integer.parseInt(n);
        }

        return number;
    }

    /**
     * Method used to calculate a subtraction expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int subtraction(String expression){
        String[] split = expression.split("[-]");
        int splitSize = split.length;
        int number = Integer.parseInt(split[0]);

        for (int i = 1; i < splitSize; i++) {
            number -= Integer.parseInt(split[i]);
        }

        return number;
    }

    /**
     * Method used to calculate a multiplication expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int multiplication(String expression) {
        String[] split = expression.split("[*]");
        int splitSize = split.length;
        int number = Integer.parseInt(split[0]);

        for (int i = 1; i < splitSize; i++) {
            number *= Integer.parseInt(split[i]);
        }

        return number;
    }


    /**
     * Method used to calculate a division expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int division(String expression) {
        String[] split = expression.split("[/]");
        int splitSize = split.length;
        int number = Integer.parseInt(split[0]);

        for (int i = 1; i < splitSize; i++) {
            int nextnumber = Integer.parseInt(split[i]);
            if (nextnumber == 0) return -1;
            number /= nextnumber;
        }
        return number;
    }

}
