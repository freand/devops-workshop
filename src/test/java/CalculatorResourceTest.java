import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+1";
        assertEquals("401", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = " 300 - 100 - 99 ";
        assertEquals("101", calculatorResource.calculate(expression));

        expression = " 300 * 2 * 2";
        assertEquals("1200", calculatorResource.calculate(expression));

        expression = " 300 / 2 / 2";
        assertEquals("75", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));

        expression = "300+99+1";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));

        expression = "20-2-2";
        assertEquals(16, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "99*10";
        assertEquals(990, calculatorResource.multiplication(expression));

        expression = "20*2";
        assertEquals(40, calculatorResource.multiplication(expression));

        expression = "20*2*2";
        assertEquals(80, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100/10";
        assertEquals(10, calculatorResource.division(expression));

        expression = "20/2";
        assertEquals(10, calculatorResource.division(expression));

        expression = "20/2/2";
        assertEquals(5, calculatorResource.division(expression));
    }
}
